﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;

namespace mobil_app.Model
{
    class Camera
    {
        public string name { get; set; }
        public bool isSoundOn { get; set; }
        public bool checkBox { get; set; }
    }

    class CameraList
    {
        public Camera[] list = Array.Empty<Camera>();
        public int count;
        
        public CameraList()
        {
            XmlReader reader = XmlReader.Create("http://demo.macroscop.com/configex?login=root&amp;password=");
            count = 0;
            string isSound;

            while (reader.Read() != false)
            {
                if (reader.Name == "ChannelInfo" && reader.NodeType == XmlNodeType.Element)
                {
                    Array.Resize(ref list, list.Length + 1);
                    list[list.Length - 1] = new Camera();

                    list[count].name = reader.GetAttribute("Name");

                    isSound = reader.GetAttribute("isSoundOn");
                    list[count].isSoundOn = isSound == "true" ? true : false;

                    list[count].checkBox = false;

                    count++;

                    reader.Skip();
                }
                else
                {
                    continue;
                }
            }
        }
    }
}
