﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using mobil_app.Model;
using mobil_app.ViewModels;

namespace mobil_app
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            var cameraListModel = new CameraList();
            InitializeComponent();
            BindingContext = new ViewModels.CameraListVM(cameraListModel);
        }

        private void OnButtonClicked(object sender, System.EventArgs e)
        {
            var cameraListModel = new CameraList();
            BindingContext = new ViewModels.CameraListVM(cameraListModel);
        }
    }
}
