﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;

using mobil_app.Model;

namespace mobil_app.ViewModels
{
    class CameraListVM
    {
        public ObservableCollection<Camera> cameraList { get; set; }

        public CameraListVM(CameraList listModel)
        {
            cameraList = new ObservableCollection<Camera>(listModel.list);
        }

    }
}
